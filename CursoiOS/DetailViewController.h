//
//  DetailViewController.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 25/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSString *informacao;

@end
