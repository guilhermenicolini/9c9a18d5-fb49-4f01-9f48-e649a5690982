//
//  ViewController.m
//  teste
//
//  Created by Guilherme Nicolini on 25/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "ViewController.h"
#import "DetailViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([sender isKindOfClass:[UIButton class]])
    {
        if ([segue.destinationViewController isKindOfClass:[DetailViewController class]])
        {
            DetailViewController *detalhes = segue.destinationViewController;
            detalhes.informacao = self.textField.text;
        }
    }
}

@end
