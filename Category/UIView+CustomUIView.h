//
//  UIView+CustomUIView.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 29/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CustomUIView)

-(void)cornerRadius:(CGFloat)radius;
-(void)addBorder:(CGFloat)borderWidth withColor:(UIColor *)color;
-(void)cornerRadius:(CGFloat)radius withBorderWidth:(CGFloat)borderWidth withColor:(UIColor *)color;
-(void)addBorder:(CGFloat)borderWidth withColor:(UIColor *)color withRadius:(CGFloat)radius;

@end
