//
//  UIView+CustomUIView.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 29/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "UIView+CustomUIView.h"

@implementation UIView (CustomUIView)

-(void)cornerRadius:(CGFloat)radius
{
    self.layer.cornerRadius = radius;
    [self.layer masksToBounds];
}

-(void)addBorder:(CGFloat)borderWidth withColor:(UIColor *)color
{
    self.layer.borderWidth = borderWidth;
    self.layer.borderColor = color.CGColor;
}

-(void)cornerRadius:(CGFloat)radius withBorderWidth:(CGFloat)borderWidth withColor:(UIColor *)color
{
    self.layer.cornerRadius = radius;
    self.layer.borderWidth = borderWidth;
    self.layer.borderColor = color.CGColor;
}

-(void)addBorder:(CGFloat)borderWidth withColor:(UIColor *)color withRadius:(CGFloat)radius
{
    self.layer.cornerRadius = radius;
    self.layer.borderWidth = borderWidth;
    self.layer.borderColor = color.CGColor;
}

@end
