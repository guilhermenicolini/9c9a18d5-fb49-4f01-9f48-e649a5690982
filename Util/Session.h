//
//  Session.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 06/12/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface Session : NSObject

@property (nonatomic, strong) Person *person;

+(instancetype)sharedSession;

@end
