//
//  ValidationHandler.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 07/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValidationHandler : NSObject

+(BOOL) IsValidEmail:(NSString *)checkString;

@end
