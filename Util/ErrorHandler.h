//
//  ErrorHandler.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 07/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorHandler : NSObject

+(void)ShowOK: (NSString*)title message:(NSString *)message;

+(void)ShowConfirm: (NSString*)title message:(NSString *)message;

+(void)ShowQuestion: (NSString*)title message:(NSString *)message;

@end
