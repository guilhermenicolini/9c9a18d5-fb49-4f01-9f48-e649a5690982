//
//  ErrorHandler.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 07/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ErrorHandler.h"

@implementation ErrorHandler

+(void)ShowOK: (NSString*)title message:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    NSLog(@"%@", message);
    [alert show];
}

+(void)ShowConfirm: (NSString*)title message:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Cancelar" otherButtonTitles: @"Confirmar", nil ];
    NSLog(@"%@", message);
    [alert show];
}

+(void)ShowQuestion: (NSString*)title message:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Não" otherButtonTitles: @"Sim", nil ];
    NSLog(@"%@", message);
    [alert show];
}

@end
