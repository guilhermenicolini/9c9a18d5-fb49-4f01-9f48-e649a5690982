//
//  Session.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 06/12/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "Session.h"
#import "Person.h"

@implementation Session

+(instancetype)sharedSession
{
    static id sharedInstance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        Session *s = [Session new];
        s.person = [Person open];
        sharedInstance = s;
    });
    
    return sharedInstance;
}

@end
