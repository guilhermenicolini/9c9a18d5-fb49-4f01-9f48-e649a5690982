//
//  ProfilePictureCC.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 29/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "ProfilePictureCC.h"

@implementation ProfilePictureCC

- (instancetype)initWithFrame:(CGRect)frame
{
    //self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"ProfilePictureCC" owner:nil options:nil] lastObject];
        self.frame = frame;
    }
    return self;
}

- (IBAction)buttonClicked:(id)sender {
    if([self.delegate respondsToSelector:@selector(profilePictureClicked)]){
        [self.delegate profilePictureClicked];
    }
}

@end
