//
//  ProfilePictureCC.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 29/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfilePictureCCDelegate <NSObject>

@required
-(void)profilePictureClicked;

@end

@interface ProfilePictureCC : UIView

@property (nonatomic, assign) id<ProfilePictureCCDelegate> delegate;

@end
