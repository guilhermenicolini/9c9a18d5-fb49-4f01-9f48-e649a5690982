//
//  GameCustomCellCC.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 13/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Plataforma.h"

@interface GameCustomCellCC : UITableViewCell

- (void) populate:(Plataforma)Plataforma Nome:(NSString *)Nome;

@end
