//
//  GameCustomCellCC.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 13/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "GameCustomCellCC.h"
#import "Game.h"

@interface GameCustomCellCC()

@property (weak, nonatomic) IBOutlet UIImageView *imgPlataforma;
@property (weak, nonatomic) IBOutlet UILabel *lblNome;

@end
@implementation GameCustomCellCC

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) populate:(Plataforma)Plataforma Nome:(NSString *)Nome;
{
    self.imgPlataforma.image = [Game getImage:Plataforma];
    self.lblNome.text = Nome;
}


@end
