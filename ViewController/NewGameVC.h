//
//  NewGameVC.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 27/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"

@protocol NewGameDelegate
-(void)SaveComplete:(Game *)game;
@end

@interface NewGameVC : UIViewController<UITextFieldDelegate>

@property (nonatomic, assign) int novoCodigo;
@property (nonatomic, weak) id NewGameDelegate;

@end
