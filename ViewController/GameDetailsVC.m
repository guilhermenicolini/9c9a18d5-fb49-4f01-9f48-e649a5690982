//
//  GameDetailsVC.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 13/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "GameDetailsVC.h"
#import <objc/runtime.h>

@interface GameDetailsVC ()

@property (weak, nonatomic) IBOutlet UIImageView *imgGame;
@property (weak, nonatomic) IBOutlet UILabel *lblNome;
@property (nonatomic) objc_property_t *propertyArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

#define MemoryCell @"memoryCell"

@end

@implementation GameDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Detalhes do Jogo";
    self.lblNome.text = @"";
    
    [self addToolBarButtons:[[self.games objectAtIndex:self.currentSection] count] > 1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnFechar_Click:(UIBarButtonItem *)sender {
    [self.IndexDelegate currentIndex:self.currentIndex withSection:self.currentSection];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnAnterior_Click:(UIBarButtonItem *)sender {
    if(--self.currentIndex < 0){
        self.currentIndex = (int)[[self.games objectAtIndex:self.currentSection] count] - 1;
    }
    [self LoadGameWithAnimation:UIViewAnimationOptionTransitionCurlDown];
}
- (IBAction)btnSeguinte_Click:(UIBarButtonItem *)sender {
    if(++self.currentIndex == [[self.games objectAtIndex:self.currentSection] count]){
        self.currentIndex = 0;
    }
    [self LoadGameWithAnimation:UIViewAnimationOptionTransitionCurlUp];
}

-(void) loadGame
{
    [self.tableView reloadData];
}

-(void) LoadGameWithAnimation:(UIViewAnimationOptions)transition
{
    [UIView transitionWithView:self.view
                      duration:0.5
                       options:transition
                    animations:^{
                        [self loadGame];
                    } completion:nil];
}

#pragma mark Table View

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    unsigned int numberOfProperties;
    self.propertyArray = class_copyPropertyList([Game class], &numberOfProperties);
    return numberOfProperties;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = MemoryCell;
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    id obj = [[self.games objectAtIndex:self.currentSection] objectAtIndex:self.currentIndex];
    objc_property_t prop = self.propertyArray[indexPath.row];
    NSString *propertyName = [[NSString alloc] initWithUTF8String:property_getName(prop)];
    NSString *propertyValue = [NSString stringWithFormat:@"%@", [obj valueForKey:[NSString stringWithUTF8String:[propertyName UTF8String]]]];
    
    cell.textLabel.text = propertyName;
    
    if([propertyName isEqualToString:@"Plataforma"]){
        cell.detailTextLabel.text = [Game getPlataforma:[propertyValue intValue]];
    }
    else if ([propertyName isEqualToString:@"Thumbnail"]){
        cell.detailTextLabel.text = @"";
    }
    else{
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", propertyValue];
    }
    
    if([propertyName isEqualToString:@"Thumbnail"]){
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[obj valueForKey:[NSString stringWithUTF8String:[propertyName UTF8String]]]];
        [imageView setContentMode: UIViewContentModeScaleAspectFit];
        [imageView setFrame:CGRectMake(0, 0, 120, 44)];
        
        cell.accessoryView = imageView;
    }

    return cell;
}

-(void)addToolBarButtons:(BOOL)navegacao
{
    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithTitle:@"Fechar" style:UIBarButtonItemStylePlain target:self action:@selector(btnFechar_Click:)];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSMutableArray *buttons = [NSMutableArray new];
    
    if(navegacao){
        UIBarButtonItem *previous = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRewind target:self action:@selector(btnAnterior_Click:)];
        [buttons addObject:previous];
        [buttons addObject:flexibleItem];
        [buttons addObject:close];
        [buttons addObject:flexibleItem];
        UIBarButtonItem *next = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFastForward target:self action:@selector(btnSeguinte_Click:)];
        [buttons addObject:next];
    }
    else{
        [buttons addObject:flexibleItem];
        [buttons addObject:close];
        [buttons addObject:flexibleItem];
    }
    
    self.toolBar.items = buttons;
}

@end
