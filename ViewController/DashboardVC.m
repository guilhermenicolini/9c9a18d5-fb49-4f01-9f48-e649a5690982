//
//  DashboardVC.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 07/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "DashboardVC.h"
#import "Person.h"
#import "Gender.h"
#import "ErrorHandler.h"
#import "LoginVC.h"
#import "GameListVC.h"
#import "ProfileViewController.h"
#import "ProgressViewController.h"

@interface DashboardVC ()
@property (weak, nonatomic) IBOutlet UILabel *lblNome;
@property (weak, nonatomic) IBOutlet UILabel *lblUsuarioId;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblDataCriacao;
@property (weak, nonatomic) IBOutlet UILabel *lblUltimoAcesso;
@property (weak, nonatomic) IBOutlet UILabel *lblIdioma;
@property (weak, nonatomic) IBOutlet UILabel *lblSexo;

@end

@implementation DashboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Dashboard";
    [self checkLogin];
    
    UIBarButtonItem *logout = [[UIBarButtonItem alloc] init];
    logout.title = @"Logout";
    logout.style = UIBarButtonItemStyleDone;
    logout.action = @selector(logout);
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    [self.navigationItem setRightBarButtonItem: rightButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnJogo_Click:(UIButton *)sender {
    GameListVC *view = [GameListVC new];
    view.Games = [Game gamesByPlataforma:self.Person.Games];
    view.AllGames = self.Person.Games;
    
    [self.navigationController pushViewController:view animated:YES];
}

-(void)checkLogin{
    
    self.session = [Session sharedSession];

    if (self.session != nil && self.session.person != nil){
        self.Person = self.session.person;
        [self loadPerson];
        return;
    }
    
    LoginVC *modal = [LoginVC new];
    modal.LoginDelegate = self;
    [self presentViewController:modal animated:NO completion:nil];
}

-(void)loadPerson{
    
    NSDateFormatter *dt = [[NSDateFormatter alloc] init];
    [dt setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    switch (self.Person.Sexo) {
        case Gender_Masculino:
            self.lblBemVindo.text = @"Bem Vindo";
            self.lblSexo.text = @"Masculino";
            break;
            
        case Gender_Feminino:
            self.lblBemVindo.text = @"Bem Vinda";
            self.lblSexo.text = @"Feminino";
            break;
    }
    self.lblNome.text = self.Person.Nome;
    self.lblUsuarioId.text = self.Person.UsuarioId;
    self.lblEmail.text = self.Person.Email;
    self.lblDataCriacao.text = [dt stringFromDate:self.Person.DataCriacao];
    self.lblUltimoAcesso.text = [dt stringFromDate:self.Person.UltimoAcesso];
    self.lblIdioma.text = self.Person.Idioma.Nome;
}

-(void)loginComplete:(Person *)person
{
    self.Person = person;
    [self loadPerson];
}

-(void)logout {
    //[ErrorHandler ShowOK:@"Teste" message:@"Logout"];
    [self.Person clear];
    [self checkLogin];
}
- (IBAction)btnProfile_Click:(id)sender {
    ProfileViewController *view = [ProfileViewController new];
    
    [self.navigationController pushViewController:view animated:YES];
}
- (IBAction)progressTouched:(id)sender {
    ProgressViewController *view = [ProgressViewController new];
    [view setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController pushViewController:view animated:YES];
}

@end
