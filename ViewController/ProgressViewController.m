//
//  ProgressViewController.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 06/12/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "ProgressViewController.h"
#import "ProgressView.h"

@interface ProgressViewController ()

@property (strong, nonatomic) IBOutlet ProgressView *mainView;
@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic, strong) NSArray *operations;

@end

@implementation ProgressViewController

#pragma mark - View Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Progresso";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.mainView setup];
}

#pragma mark - Overrides
-(ProgressView *)mainView
{
    ProgressView *view = (ProgressView *) self.view;
    return view;
}

-(NSOperationQueue *)queue
{
    if (!_queue){
        _queue = [NSOperationQueue new];
    }
    return _queue;
}

#pragma mark - Actions

- (IBAction)startThread:(id)sender {
    [self.queue cancelAllOperations];
    [self.mainView resetAllProgress];
    [self createOperations];
}

#pragma mark - Methods

-(NSBlockOperation *)addOperationWithProgressView:(UIProgressView *)progressView andFinalCount:(NSInteger)count andTimeInterval:(CGFloat)interval
{
    NSBlockOperation * operation = [NSBlockOperation new];
    
    __weak NSBlockOperation *weakOperation = operation;
    
    [operation addExecutionBlock:^{
        for(int i = 1; i <= count; i++)
        {
            int random = arc4random() % 15;
            if(random == 4){
                [weakOperation cancel];
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self.mainView cancelProgress:progressView];
                }];
                break;
            }
            
            [NSThread sleepForTimeInterval:interval];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self.mainView setProgress:progressView withInterval:((CGFloat) i/count)];
            }];
        };
    }];
    
    return operation;
}

-(void)createOperations
{
    NSBlockOperation *operation1 = [self addOperationWithProgressView:[self.mainView getProgressView:1] andFinalCount:12 andTimeInterval:1.2F];
    NSBlockOperation *operation2 = [self addOperationWithProgressView:[self.mainView getProgressView:2] andFinalCount:20 andTimeInterval:0.8F];
    NSBlockOperation *operation3 = [self addOperationWithProgressView:[self.mainView getProgressView:3] andFinalCount:27 andTimeInterval:1.5F];
    NSBlockOperation *operation4 = [self addOperationWithProgressView:[self.mainView getProgressView:4] andFinalCount:8 andTimeInterval:2.0F];
    NSBlockOperation *lastOperation = [NSBlockOperation new];
    
    [lastOperation addExecutionBlock:^{
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Done" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
    }];
    
    self.operations = @[ operation1, operation2, operation3, operation4, lastOperation];
    self.queue.maxConcurrentOperationCount = 4;
    
    [lastOperation addDependency:operation1];
    [lastOperation addDependency:operation2];
    [lastOperation addDependency:operation3];
    [lastOperation addDependency:operation4];
    
    [self.queue addOperations:self.operations waitUntilFinished:NO];
}

@end
