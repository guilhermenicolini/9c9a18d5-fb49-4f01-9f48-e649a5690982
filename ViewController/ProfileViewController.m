//
//  ProfileViewController.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 29/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "ProfileViewController.h"
#import "ProfileView.h"

@interface ProfileViewController ()
@property (strong, nonatomic) IBOutlet ProfileView *mainView;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Perfil";
    
    [self doSomethingWithBlock:^(NSInteger value) {
        NSLog(@"Result block %i", (int)value);
    }];
    
    int test = 10;
    
    void (^testBlock)(void) = ^{
        NSLog(@"Test = %d", test);
    };
    
    test = 20;
    testBlock();
    NSLog(@"Fora do Block = %d", test);
}

-(void)viewDidAppear:(BOOL)animated
{
    //[NSThread sleepForTimeInterval:5];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.mainView setup];
    [self.mainView addProfilePicture:self];
    [self.mainView setResultToLabel:@""];
}

#pragma mark - Override UIView
-(ProfileView *)mainView
{
    ProfileView *view = (ProfileView *) self.view;
    return view;
}

#pragma mark - ProfilePictureCCDelegate

-(void)profilePictureClicked{
    [self.mainView setResultToLabel:@"Botão foi clicado."];
}

#pragma mark - methods with block
- (void) doSomethingWithBlock:(void (^)(NSInteger value)) completionBlock
{
    NSLog(@"Block");
    NSInteger value = 10;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [NSThread sleepForTimeInterval:5];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thread" message:@"Thead terminou" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    });
    
    completionBlock(value);
}

@end
