//
//  GameZoomVC.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 26/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameZoomVC : UIViewController<UIScrollViewDelegate>

@property (nonatomic, strong) UIImage *image;

@end
