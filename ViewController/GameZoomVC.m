//
//  GameZoomVC.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 26/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "GameZoomVC.h"

@interface GameZoomVC ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *gameImageView;

@end

@implementation GameZoomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Zoom";
    
    self.gameImageView = [[UIImageView alloc] initWithImage:self.image];
    self.scrollView.contentSize = self.gameImageView.frame.size;
    [self.scrollView addSubview:self.gameImageView];
    self.scrollView.delegate = self;
    self.scrollView.maximumZoomScale = 2.0;
    self.scrollView.minimumZoomScale = 0.5;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.gameImageView;
}

@end
