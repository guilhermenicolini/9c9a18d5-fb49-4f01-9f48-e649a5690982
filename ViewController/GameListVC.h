//
//  GameListVC.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 12/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameDetailsVC.h"
#import "NewGameVC.h"

@interface GameListVC : UIViewController<UITableViewDelegate, UITableViewDataSource, IndexDelegate, NewGameDelegate>

@property (nonatomic, strong) NSArray *Games;
@property (nonatomic, strong) NSArray *AllGames;

@end
