//
//  GameDetailsVC.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 13/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"

@protocol IndexDelegate
-(void)currentIndex:(int)index withSection:(int)section;
@end

@interface GameDetailsVC : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id IndexDelegate;
@property (nonatomic, strong) NSArray *games;
@property (nonatomic, assign) int currentIndex;
@property (nonatomic, assign) int currentSection;

@end
