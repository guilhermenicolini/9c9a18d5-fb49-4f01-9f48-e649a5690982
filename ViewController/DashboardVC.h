//
//  DashboardVC.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 07/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"
#import "LoginVC.h"
#import "Session.h"

@interface DashboardVC : UIViewController<LoginDelegate>

@property (nonatomic, strong) Person *Person;
@property (weak, nonatomic) IBOutlet UILabel *lblBemVindo;
@property (nonatomic, strong) Session *session;

@end
