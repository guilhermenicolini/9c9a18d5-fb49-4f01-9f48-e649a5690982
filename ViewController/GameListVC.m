//
//  GameListVC.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 12/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "GameListVC.h"
#import "Game.h"
#import "GameCustomCellCC.h"
#import "GameDetailsVC.h"
#import "ErrorHandler.h"
#import "GameZoomVC.h"
#import "NewGameVC.h"

@interface GameListVC ()
@property (weak, nonatomic) IBOutlet UITableView *gridGames;

#define MemoryCell @"memoryCell"

@end

@implementation GameListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Meus Jogos";
    
    [self.gridGames registerNib:[UINib nibWithNibName:@"GameCustomCellCC"
        bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:MemoryCell];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(Add)];
}

-(IBAction)Add
{
    NewGameVC *push = [NewGameVC new];
    push.novoCodigo = [Game getNovoCodigo:self.AllGames];
    push.NewGameDelegate = self;
    [self.navigationController pushViewController:push animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.Games count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    Game *firstGame = [[self.Games objectAtIndex:section] objectAtIndex:0];
    
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor whiteColor];
    label.font = [label.font fontWithSize:15];
    label.text = [Game getPlataforma:firstGame.Plataforma];
    label.textAlignment = NSTextAlignmentCenter;
        
    return label;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[self.Games objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = MemoryCell;
    
    GameCustomCellCC *cell = (GameCustomCellCC *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    Game *game = [[self.Games objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    [cell populate:game.Plataforma Nome:game.Nome];
    return cell;
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    GameDetailsVC *modal = [GameDetailsVC new];
    modal.IndexDelegate = self;
    modal.games = self.Games;
    modal.currentIndex = (int)indexPath.row;
    modal.currentSection = (int)indexPath.section;
    [modal setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self presentViewController:modal animated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Game *game = [[self.Games objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    GameZoomVC *push = [GameZoomVC new];
    push.image = game.Thumbnail;
    [self.navigationController pushViewController:push animated:YES];
}

- (void)currentIndex:(int)index withSection:(int)section;
{
    [self.gridGames selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:section] animated:YES scrollPosition:UITableViewScrollPositionTop];
}

-(void)SaveComplete:(Game *)game
{
    NSMutableArray *tmp = [NSMutableArray new];
    [tmp addObjectsFromArray:self.AllGames];
    [tmp addObject:game];
    self.AllGames = tmp;
    self.Games = [Game gamesByPlataforma:self.AllGames];
    [self.gridGames reloadData];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSUInteger oldSectionNumber = [self.Games count];
        
        NSMutableArray *allGames = [[NSMutableArray alloc] initWithArray:self.AllGames];
        Game *game = [[self.Games objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        [allGames removeObject:game];
        self.AllGames = allGames;
        self.Games = [Game gamesByPlataforma:allGames];
        NSUInteger newSectionNumber = [self.Games count];
        
        if(oldSectionNumber == newSectionNumber){
            [self.gridGames deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        else{
            [self.gridGames deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

@end
