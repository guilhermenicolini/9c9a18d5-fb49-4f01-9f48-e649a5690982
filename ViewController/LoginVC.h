//
//  LoginVC.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 07/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@protocol LoginDelegate
-(void)loginComplete:(Person *)person;
@end

@interface LoginVC : UIViewController

@property (nonatomic, weak) id LoginDelegate;

@end
