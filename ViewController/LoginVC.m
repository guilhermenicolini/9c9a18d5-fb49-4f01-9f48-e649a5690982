//
//  LoginVC.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 07/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "LoginVC.h"
#import "Person.h"
#import "ErrorHandler.h"
#import "ValidationHandler.h"
#import "DashboardVC.h"

@interface LoginVC ()

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtSenha;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Login";
    
    self.txtEmail.text = @"guilhermenicolini@gmail.com";
    self.txtSenha.text = @"123456";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnLogin_Click:(id)sender{
    
    //Validar e-mail preenchido
    if (self.txtEmail.text.length == 0){
        [ErrorHandler ShowOK:@"Erro" message:@"Coloque o e-mail."];
        [self.txtEmail becomeFirstResponder];
        return;
    }
    
    //Validar formato e-mail
    if(![ValidationHandler IsValidEmail:self.txtEmail.text]) {
        [ErrorHandler ShowOK:@"Erro" message:@"E-mail inválido."];
        [self.txtEmail becomeFirstResponder];
        return;
    }
    
    //Validar senha preenchida
    if (self.txtSenha.text.length == 0){
        [ErrorHandler ShowOK:@"Erro" message:@"Coloque a senha."];
        [self.txtSenha becomeFirstResponder];
        return;
    }
    
    NSArray *persons = Person.Carregar;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Email = %@ AND Senha = %@", self.txtEmail.text, self.txtSenha.text];
    
    NSArray *filtered = [persons filteredArrayUsingPredicate:predicate];
    
    if(filtered.count != 1){
        [ErrorHandler ShowOK:@"Erro" message:@"E-mail ou senha inválida."];
        self.txtSenha.text = @"";
        [self.txtEmail becomeFirstResponder];
        return;
    }
    
    Person *tmpPerson = filtered[0];
    
    if (tmpPerson == nil){
        [ErrorHandler ShowOK:@"Erro" message:@"Usuário inváido."];
        self.txtSenha.text = @"";
        [self.txtEmail becomeFirstResponder];
        return;
    }
    
    [tmpPerson encode];
    
    [self.LoginDelegate loginComplete:tmpPerson];
    [self dismissViewControllerAnimated:YES completion:nil];
    
//    DashboardVC *dash = [DashboardVC new];
//    dash.Person = filtered[0];
//    
//    [self.navigationController pushViewController:dash animated:YES];
    
}

@end
