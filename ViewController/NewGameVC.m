//
//  NewGameVC.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 27/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "NewGameVC.h"
#import "ErrorHandler.h"
#import "Game.h"

@interface NewGameVC ()
@property (weak, nonatomic) IBOutlet UITextField *txtCodigo;
@property (weak, nonatomic) IBOutlet UITextField *txtPlataforma;
@property (weak, nonatomic) IBOutlet UITextField *txtNome;
@property (weak, nonatomic) IBOutlet UITextField *txtAno;
@property (weak, nonatomic) IBOutlet UITextField *txtDesenvolvedor;
@property (weak, nonatomic) IBOutlet UITextField *txtGenero;
@property (weak, nonatomic) IBOutlet UITextField *txtImagemUrl;
@end

@implementation NewGameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Novo Jogo";
    
    self.txtCodigo.text = [NSString stringWithFormat:@"%i", self.novoCodigo];
    
    self.txtPlataforma.delegate = self;
    self.txtNome.delegate = self;
    self.txtAno.delegate = self;
    self.txtDesenvolvedor.delegate = self;
    self.txtGenero.delegate = self;
    self.txtImagemUrl.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCancelar_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnSalvar_Click:(id)sender {
    //Validar plataforma preenchida
    if (self.txtPlataforma.text.length == 0){
        [ErrorHandler ShowOK:@"Erro" message:@"Selecione a plataforma"];
        [self.txtPlataforma becomeFirstResponder];
        return;
    }
    
    //Validar nome preenchido
    if (self.txtNome.text.length == 0){
        [ErrorHandler ShowOK:@"Erro" message:@"Coloque o nome"];
        [self.txtNome becomeFirstResponder];
        return;
    }
    
    //Validar ano preenchido
    if (self.txtAno.text.length == 0){
        [ErrorHandler ShowOK:@"Erro" message:@"Coloque o ano"];
        [self.txtAno becomeFirstResponder];
        return;
    }
    
    //Validar desenvolvedor preenchido
    if (self.txtDesenvolvedor.text.length == 0){
        [ErrorHandler ShowOK:@"Erro" message:@"Coloque o desenvolvedor"];
        [self.txtDesenvolvedor becomeFirstResponder];
        return;
    }
    
    //Validar genero preenchido
    if (self.txtGenero.text.length == 0){
        [ErrorHandler ShowOK:@"Erro" message:@"Coloque o genero"];
        [self.txtGenero becomeFirstResponder];
        return;
    }
    
    //Validar genero preenchido
    if (self.txtImagemUrl.text.length == 0){
        [ErrorHandler ShowOK:@"Erro" message:@"Coloque a imagem"];
        [self.txtImagemUrl becomeFirstResponder];
        return;
    }
    
    Game *game = [Game new];
    game.Codigo = self.novoCodigo;
    game.Plataforma = [self.txtPlataforma.text intValue];
    game.Nome = self.txtNome.text;
    game.Ano = [self.txtAno.text intValue];
    game.Desenvolvedor = self.txtDesenvolvedor.text;
    game.Genero = self.txtGenero.text;
    
    NSUInteger lastIndex = [self.txtImagemUrl.text rangeOfString:@"/" options:NSBackwardsSearch].location + 1;
    NSUInteger lenght = self.txtImagemUrl.text.length - lastIndex;
    
    game.Imagem = [self.txtImagemUrl.text substringWithRange:NSMakeRange(lastIndex,lenght)];

    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: self.txtImagemUrl.text]];
    game.Thumbnail = [UIImage imageWithData: imageData];
    
    [self.NewGameDelegate SaveComplete:game];
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
