//
//  ProfileView.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 29/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "ProfileView.h"
#import "ProfilePictureCC.h"
#import "UIView+CustomUIView.h"

@interface ProfileView ()

@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation ProfileView

-(void)setup
{
    self.backgroundColor = [UIColor blackColor];
    self.label.textColor = [UIColor whiteColor];
}

-(void)addProfilePicture:(id<ProfilePictureCCDelegate>)delegate
{
    CGRect frame = CGRectMake((self.frame.size.width / 2) - 50, (self.frame.size.height / 2) - 50, 100, 100);
    ProfilePictureCC *view = [[ProfilePictureCC alloc] initWithFrame:frame];
    [view cornerRadius:50];
    [view addBorder:4 withColor:[UIColor redColor]];
    view.delegate = delegate;
    [self addSubview:view];
}

-(void)setResultToLabel:(NSString *)text
{
    self.label.text = text;
}

@end
