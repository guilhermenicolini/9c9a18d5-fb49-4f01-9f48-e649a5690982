//
//  ProgressView.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 06/12/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView

-(UIProgressView *)getProgressView:(int)progressId;
-(void)setup;
-(void)cancelProgress:(UIProgressView *)progress;
-(void)setProgress:(UIProgressView *)progress withInterval:(CGFloat)internal;
-(void)resetAllProgress;
@end
