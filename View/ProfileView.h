//
//  ProfileView.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 29/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfilePictureCC.h"

@interface ProfileView : UIView

-(void)setup;
-(void)addProfilePicture:(id<ProfilePictureCCDelegate>) delegate;
-(void)setResultToLabel:(NSString *)text;

@end

