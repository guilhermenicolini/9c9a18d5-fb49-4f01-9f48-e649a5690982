//
//  ProgressView.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 06/12/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//



#import "ProgressView.h"

@interface ProgressView()
@property (weak, nonatomic) IBOutlet UIProgressView *progressView1;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView2;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView3;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView4;
@property (strong, nonatomic) IBOutletCollection(UIProgressView) NSArray *progressCollection;

@end

@implementation ProgressView

-(void)setup
{
    self.backgroundColor = [UIColor grayColor];
    
    CGAffineTransform transform =CGAffineTransformMakeScale(1.0F, 10.0F);
    
    for (UIProgressView *progress in self.progressCollection) {
        [progress setTransform:transform];
        
        progress.progressTintColor = [UIColor greenColor];
        progress.trackTintColor = [UIColor whiteColor];
        progress.progress = 0;
    };
}

-(UIProgressView *)getProgressView:(int)progressId
{
    switch (progressId) {
        case 1:
            return self.progressView1;
            break;
        case 2:
            return self.progressView2;
            break;
        case 3:
            return self.progressView3;
            break;
        case 4:
            return self.progressView4;
            break;
        default:
            return nil;
    }
}

-(void)cancelProgress:(UIProgressView *)progress
{
    progress.progress = 0;
    progress.trackTintColor = [UIColor redColor];
}

-(void)setProgress:(UIProgressView *)progress withInterval:(CGFloat)internal
{
    progress.progress = internal;
}

-(void)resetAllProgress
{
    for (UIProgressView *progress in self.progressCollection) {
        
        progress.progressTintColor = [UIColor greenColor];
        progress.trackTintColor = [UIColor whiteColor];
        progress.progress = 0;
    };
}

@end
