//
//  Language.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 07/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Language : NSObject

@property (nonatomic, assign) int IdiomaId;

@property (nonatomic, strong) NSString *Nome;

+ (NSArray*)Carregar;

- (instancetype) initFull:(int)IdiomaId second:(NSString *)Nome;

@end
