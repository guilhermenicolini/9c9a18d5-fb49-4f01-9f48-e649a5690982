//
//  Gender.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 07/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    Gender_Masculino = 0,
    Gender_Feminino = 1
} Gender;
