//
//  Game.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 12/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Plataforma.h"

@interface Game : NSObject

@property (nonatomic, assign) int Codigo;
@property (nonatomic, assign) Plataforma Plataforma;
@property (nonatomic, strong) NSString *Nome;
@property (nonatomic, strong) NSString *Imagem;
@property (nonatomic, strong) UIImage *Thumbnail;
@property (nonatomic, assign) int Ano;
@property (nonatomic, strong) NSString *Desenvolvedor;
@property (nonatomic, strong) NSString *Genero;

-(instancetype) initFull:(int)Codigo Plataforma:(Plataforma)Plataforma Nome:(NSString *)Nome Imagem:(NSString *)Imagem;
+(NSArray *) Carregar;
+(UIImage *) getImage:(Plataforma)Plataforma;
+(NSArray *) getRandom:(NSArray *)Array NumberOfItems:(int)NumberOfItems;
+(NSString *) getPlataforma:(Plataforma)Plataforma;
+(NSArray *)gamesByPlataforma:(NSArray *)games;
+(NSArray *)gamesInPlataforma:(NSArray *)games withPlataforma:(Plataforma)plataforma;
+(int)getNovoCodigo:(NSArray *)games;
@end
