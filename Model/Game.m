//
//  Game.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 12/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "Game.h"

@implementation Game

- (instancetype) initFull:(int)Codigo Plataforma:(Plataforma)Plataforma Nome:(NSString *)Nome Imagem:(NSString *)Imagem{
    
    if(self=[super init])
    {
        self.Codigo = Codigo;
        self.Plataforma = Plataforma;
        self.Nome = Nome;
        self.Imagem = Imagem;
        self.Thumbnail = [UIImage imageNamed:Imagem];
    }
    
    return self;
}

+ (NSArray*)Carregar
{
    NSMutableArray *games = [NSMutableArray new];
    
    Game *g1 = [Game new];
    g1.Codigo = 1;
    g1.Plataforma = Plataforma_PS3;
    g1.nome = @"Uncharted 3: Drake's Deception";
    g1.Imagem = @"ps3_001.jpg";
    g1.Thumbnail = [UIImage imageNamed:g1.Imagem];
    g1.Ano = 2011;
    g1.Desenvolvedor = @"Naughty Dog";
    g1.Genero = @"Ação/Aventura";
    [games addObject:g1];
    
    Game *g2 = [Game new];
    g2.Codigo = 2;
    g2.Plataforma = Plataforma_PS3;
    g2.nome = @"The Last of Us";
    g2.Imagem = @"ps3_002.jpg";
    g2.Thumbnail = [UIImage imageNamed:g2.Imagem];
    g2.Ano = 2013;
    g2.Desenvolvedor = @"Naughty Dog";
    g2.Genero = @"Ação/Aventura";
    [games addObject:g2];
    
    Game *g3 = [Game new];
    g3.Codigo = 3;
    g3.Plataforma = Plataforma_PS3;
    g3.nome = @"LittleBigPlanet 3";
    g3.Imagem = @"ps3_003.jpg";
    g3.Thumbnail = [UIImage imageNamed:g3.Imagem];
    g3.Ano = 2014;
    g3.Desenvolvedor = @"Sumo Digital";
    g3.Genero = @"Aventura/Plataforma";
    [games addObject:g3];
    
    Game *g4 = [Game new];
    g4.Codigo = 4;
    g4.Plataforma = Plataforma_PS3;
    g4.nome = @"Destiny";
    g4.Imagem = @"ps3_004.jpg";
    g4.Thumbnail = [UIImage imageNamed:g4.Imagem];
    g4.Ano = 2014;
    g4.Desenvolvedor = @"Bungie";
    g4.Genero = @"Ação/Aventura/Tiro em Primeira Pessoa";
    [games addObject:g4];
    
    Game *g5 = [Game new];
    g5.Codigo = 5;
    g5.Plataforma = Plataforma_PS3;
    g5.nome = @"Dragon Age: Inquisition";
    g5.Imagem = @"ps3_005.jpg";
    g5.Thumbnail = [UIImage imageNamed:g5.Imagem];
    g5.Ano = 2014;
    g5.Desenvolvedor = @"BioWare";
    g5.Genero = @"RPG/Ação";
    [games addObject:g5];
    
    Game *g6 = [Game new];
    g6.Codigo = 6;
    g6.Plataforma = Plataforma_XBox;
    g6.nome = @"Far Cry 4";
    g6.Imagem = @"xbox_001.jpg";
    g6.Thumbnail = [UIImage imageNamed:g6.Imagem];
    g6.Ano = 2014;
    g6.Desenvolvedor = @"Ubisoft";
    g6.Genero = @"Tiro";
    [games addObject:g6];
    
    Game *g7 = [Game new];
    g7.Codigo = 7;
    g7.Plataforma = Plataforma_XBox;
    g7.nome = @"Titanfall IMC Rising";
    g7.Imagem = @"xbox_002.jpg";
    g7.Thumbnail = [UIImage imageNamed:g7.Imagem];
    g7.Ano = 2014;
    g7.Desenvolvedor = @"Respawn Entertainment";
    g7.Genero = @"Ação/Aventura/Tiro";
    [games addObject:g7];
    
    Game *g8 = [Game new];
    g8.Codigo = 8;
    g8.Plataforma = Plataforma_XBox;
    g8.nome = @"Watch Dogs";
    g8.Imagem = @"xbox_003.jpg";
    g8.Thumbnail = [UIImage imageNamed:g8.Imagem];
    g8.Ano = 2014;
    g8.Desenvolvedor = @"Ubisoft";
    g8.Genero = @"Ação/Aventura";
    [games addObject:g8];
    
    Game *g9 = [Game new];
    g9.Codigo = 9;
    g9.Plataforma = Plataforma_XBox;
    g9.nome = @"Batman: Arkham Origins";
    g9.Imagem = @"xbox_004.jpg";
    g9.Thumbnail = [UIImage imageNamed:g9.Imagem];
    g9.Ano = 2014;
    g9.Desenvolvedor = @"WB Games Montréal";
    g9.Genero = @"Ação/Aventura";
    [games addObject:g9];
    
    Game *g10 = [Game new];
    g10.Codigo = 10;
    g10.Plataforma = Plataforma_XBox;
    g10.nome = @"DBZ: Battle of Z";
    g10.Imagem = @"xbox_005.jpg";
    g10.Thumbnail = [UIImage imageNamed:g10.Imagem];
    g10.Ano = 2014;
    g10.Desenvolvedor = @"ARTDINK CORPORATION";
    g10.Genero = @"Ação/Aventura/Luta";
    [games addObject:g10];
    
    Game *g11 = [Game new];
    g11.Codigo = 11;
    g11.Plataforma = Plataforma_WiiU;
    g11.nome = @"Super Smash Bros. Bundle";
    g11.Imagem = @"wiiu_001.jpg";
    g11.Thumbnail = [UIImage imageNamed:g11.Imagem];
    g11.Ano = 2014;
    g11.Desenvolvedor = @"Sora Ltd. / BANDAI NAMCO Studios Inc.";
    g11.Genero = @"Ação";
    [games addObject:g11];
    
    Game *g12 = [Game new];
    g12.Codigo = 12;
    g12.Plataforma = Plataforma_WiiU;
    g12.nome = @"Mario Kart 8";
    g12.Imagem = @"wiiu_002.jpg";
    g12.Thumbnail = [UIImage imageNamed:g12.Imagem];
    g12.Ano = 2014;
    g12.Desenvolvedor = @"Nintendo";
    g12.Genero = @"Corrida";
    [games addObject:g12];
    
    Game *g13 = [Game new];
    g13.Codigo = 13;
    g13.Plataforma = Plataforma_WiiU;
    g13.nome = @"Hyrule Warriors";
    g13.Imagem = @"wiiu_003.jpg";
    g13.Thumbnail = [UIImage imageNamed:g13.Imagem];
    g13.Ano = 2014;
    g13.Desenvolvedor = @"Tecmo Koei Games Co., Ltd.";
    g13.Genero = @"Ação/Aventura";
    [games addObject:g13];
    
    Game *g14 = [Game new];
    g14.Codigo = 14;
    g14.Plataforma = Plataforma_WiiU;
    g14.nome = @"Bayonetta 2";
    g14.Imagem = @"wiiu_004.jpg";
    g14.Thumbnail = [UIImage imageNamed:g14.Imagem];
    g14.Ano = 2014;
    g14.Desenvolvedor = @"PlatinumGames Inc";
    g14.Genero = @"Ação";
    [games addObject:g14];
    
    Game *g15 = [Game new];
    g15.Codigo = 15;
    g15.Plataforma = Plataforma_WiiU;
    g15.nome = @"Sonic Boom: Rise of Lyric";
    g15.Imagem = @"wiiu_005.jpg";
    g15.Thumbnail = [UIImage imageNamed:g15.Imagem];
    g15.Ano = 2014;
    g15.Desenvolvedor = @"Big Red Button Entertainment";
    g15.Genero = @"Ação/Aventura";
    [games addObject:g15];
    
    return games;
}

+(UIImage *) getImage:(Plataforma)Plataforma
{
    switch (Plataforma) {
        case Plataforma_PS3:
            return [UIImage imageNamed:@"ps3_logo.png"];
            break;
        case Plataforma_XBox:
            return [UIImage imageNamed:@"xbox_logo.png"];
            break;
        case Plataforma_WiiU:
            return [UIImage imageNamed:@"wiiu_logo.png"];
        case Plataforma_N64:
            return [UIImage imageNamed:@"n64_logo.png"];
        default:
            return [UIImage imageNamed:@"no_logo.png"];
            break;
    }
}

+(NSArray *) getRandom:(NSArray *)Array NumberOfItems:(int)NumberOfItems
{
    NSMutableArray *games = [NSMutableArray new];
    
    for(int i = 1; i <= NumberOfItems; i++){
        int randomIndex = arc4random() % [Array count];
        Game *tmpGame = [Array objectAtIndex:randomIndex];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Codigo = %i", tmpGame.Codigo];
        NSArray *filtered = [games filteredArrayUsingPredicate:predicate];
        if([filtered count] == 1){
            i--;
        }
        else{
            [games addObject:tmpGame];
        }
    }
    return games;
}

+(NSString *) getPlataforma:(Plataforma)Plataforma
{
    switch (Plataforma) {
        case Plataforma_PS3:
            return @"Playstation 3";
            break;
        case Plataforma_XBox:
            return @"Xbox 360";
            break;
        case Plataforma_WiiU:
            return @"Nintendo Wii U";
        case Plataforma_N64:
            return @"Nintendo 64";
            break;
        default:
            return @"Desconhecido";
            break;
    }
}

+(NSArray *)gamesByPlataforma:(NSArray *)games
{
    NSMutableArray *listOfGamesByPlataforma = [NSMutableArray new];
    
    for(int i = 0; i < Plataforma_Count; i++)
    {
        NSArray *tmp = [self gamesInPlataforma:games withPlataforma:i];
        if ([tmp count] > 0){
            [listOfGamesByPlataforma addObject:tmp];
        }
    }
    
    return listOfGamesByPlataforma;
}

+(NSArray *)gamesInPlataforma:(NSArray *)games withPlataforma:(Plataforma)plataforma
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Plataforma=%i", plataforma];
    NSArray *filtered = [games filteredArrayUsingPredicate:predicate];
    return filtered;
}

+(int)getNovoCodigo:(NSArray *)games
{
    if(!games || [games count] == 0){
        return 1;
    }
    
    //Ordernar lista pelo código
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Codigo" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [games sortedArrayUsingDescriptors:sortDescriptors];
    
    int novoCodigo = 0;
    Game *ultimoGame = sortedArray[[sortedArray count] - 1];
    for(Game* game in sortedArray){
        //Se chegou no último Código sem saltar
        if (game.Codigo == ultimoGame.Codigo){
            novoCodigo = game.Codigo + 1;
            break;
        }
        //Se o novoCodigo for o numero exatamente anterior, entao não existe salto de Código
        else if (novoCodigo == game.Codigo - 1){
            novoCodigo = game.Codigo;
            continue;
        }
        //Se for diferente, então existe salto de Código
        else{
            novoCodigo += 1;
            break;
        }
    }
    
    return novoCodigo;
}

@end
