//
//  Plataforma.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 12/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

typedef NS_ENUM(NSUInteger, Plataforma) {
    Plataforma_PS3 = 0,
    Plataforma_XBox = 1,
    Plataforma_WiiU = 2,
    Plataforma_N64 = 3,
    Plataforma_Count = 4
};
