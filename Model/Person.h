//
//  Person.h
//  CursoiOS
//
//  Created by Guilherme Nicolini on 01/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Language.h"
#import "Gender.h"

@interface Person : NSObject

@property (nonatomic, strong) NSString *UsuarioId;

@property (nonatomic, strong) NSString *Nome;

@property (nonatomic, strong) NSString *Email;

@property (nonatomic, strong) NSString *Senha;

@property (nonatomic, strong) NSDate *DataCriacao;

@property (nonatomic, strong) NSDate *UltimoAcesso;

@property (nonatomic, strong) Language *Idioma;

@property (nonatomic, assign) Gender Sexo;

@property (nonatomic, strong) NSArray *Games;

+ (NSArray*)Carregar;

-(void)encode;

-(void)clear;

+ (Person*) open;
@end
