//
//  Language.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 07/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "Language.h"

@implementation Language

+ (NSArray*)Carregar{
    NSMutableArray *languages = [NSMutableArray new];
    [languages addObject: [[Language alloc] initFull:1 second: @"Português (Brasil)"]];
    [languages addObject: [[Language alloc] initFull: 2 second: @"Inglês (Estados Unidos)"]];
    return languages;
}

- (instancetype) initFull:(int)IdiomaId second:(NSString *)Nome{
    
    if(self=[super init])
    {
        self.IdiomaId = IdiomaId;
        self.Nome = Nome;
    }
    
    return self;
}

@end
