//
//  Person.m
//  CursoiOS
//
//  Created by Guilherme Nicolini on 01/11/14.
//  Copyright (c) 2014 Oxus Desenvolvimento. All rights reserved.
//

#import "Person.h"
#import "Language.h"
#import "Gender.h"
#import "Game.h"

@implementation Person

+ (NSArray*)Carregar{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSArray *languages = Language.Carregar;
    NSArray *games = [Game Carregar];
    
    NSMutableArray *persons = [NSMutableArray new];
    
    Person *p1 = [Person new];
    p1.UsuarioId = @"92693dca-853b-4f1f-aba9-5ea691fb4e23";
    p1.Nome = @"Guilherme Nicolini";
    p1.Email = @"guilhermenicolini@gmail.com";
    
    //p1.Senha = @"e10adc3949ba59abbe56e057f20f883e"
    p1.Senha = @"123456";
    p1.DataCriacao = [df dateFromString: @"2013-04-09 16:34"];
    p1.UltimoAcesso = [NSDate date];
    p1.Idioma = languages[0];
    p1.Sexo = Gender_Masculino;
    p1.Games = [Game getRandom:games NumberOfItems:15];
    [persons addObject: p1];
    
    return persons;
}

-(void)encode{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [standardUserDefaults setObject:self.UsuarioId forKey:@"Session.Person.UsuarioId"];
    [standardUserDefaults synchronize];
}

-(void)clear{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [standardUserDefaults setObject:nil forKey:@"Session.Person.UsuarioId"];
    [standardUserDefaults synchronize];
}

+ (Person*) open {

    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *usuarioId = [standardUserDefaults stringForKey:@"Session.Person.UsuarioId"];
    
    if(usuarioId == nil)
        return nil;

    NSArray *persons = Person.Carregar;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"UsuarioId = %@", usuarioId];
    
    NSArray *filtered = [persons filteredArrayUsingPredicate:predicate];
    
    if(filtered.count != 1){
        return nil;
    }
    
    return filtered[0];
}

@end
